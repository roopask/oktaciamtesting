import * as cdk from 'aws-cdk-lib';
import { aws_s3 as s3 } from 'aws-cdk-lib';

export class HelloCdkStack extends cdk.Stack {
  constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const bucketname = `${process.env.BUCKETNAME}`
    new s3.Bucket(this,bucketname, {
      versioned: true
    });
  }
}